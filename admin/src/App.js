import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Admin, Resource } from 'react-admin';
import loopbackRestClient, {authClient} from 'aor-loopback';
import { UserList, UserCreate, UserEdit, UserShow } from './resources/user';
import { ProjectList, ProjectCreate, ProjectEdit, ProjectShow } from './resources/project';
import { TaskList, TaskCreate, TaskEdit, TaskShow } from './resources/task';
class App extends Component {
  render() {
    return (
      <Admin dataProvider={loopbackRestClient('http://localhost:3000/api')} authProvider={authClient('http://localhost:3000/api/AppUsers/login')} >
        <Resource name="appusers" list={UserList} create={UserCreate} edit={UserEdit} show={UserShow} ></Resource>
        <Resource name="projects" list={ProjectList} create={ProjectCreate} edit={ProjectEdit} show={ProjectShow} ></Resource>
        <Resource name="tasks" list={TaskList} create={TaskCreate} edit={TaskEdit} show={TaskShow} ></Resource>
      </Admin>
    );
  }
}

export default App;
