import React from 'react';
import {
  List,
  Datagrid,
  TextField,
  Create,
  SimpleForm,
  TextInput,
  Edit,
  EditButton,
  SimpleShowLayout,
  Show,
  ShowButton,
  DisabledInput,
  ReferenceInput,
  SelectInput,
  ReferenceField,
  ChipField,
  FunctionField,
  Filter,
  BooleanInput
} from 'react-admin'
import DoneButton from './task/DoneButton';
import IncompleteButton from './task/IncompleteButton';

const TaskFilter = (props) => (
  <Filter {...props}>
    <BooleanInput source="isDone" />
    <TextInput source="name" parse={v => ({like: `.*${v}.*`})} format={v=> v ? v.like.replace(/\.\*/g, '') : ''}/>
  </Filter>
);

export const TaskList = (props) => (
  <List {...props} filters={<TaskFilter/>} perPage={10}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <FunctionField label="Status" render={record => (<span style={{color: record.isDone ? 'green': 'red'}}>{record.isDone ? 'Complete': 'Incomplete'}</span>)}/>
      <ReferenceField label="Project" source="projectId" reference="projects" linkType="show">
        <ChipField source="name" />
      </ReferenceField>
      <FunctionField label="Button" render={record => (record.isDone ? <IncompleteButton record={record}></IncompleteButton> : <DoneButton record={record}></DoneButton>)}></FunctionField>
      <EditButton></EditButton>
      <ShowButton></ShowButton>
    </Datagrid>
  </List>
)

const validateTask = (values) => {
  const errors = {};
  if (!values.name) {
      errors.name = ['The name is required'];
  }
  return errors
};
export const TaskCreate = (props) => (
  <Create {...props}>
    <SimpleForm validate={validateTask}>
      <TextInput source="name" />
      <TextInput source="description" />
      <ReferenceInput label="Project" source="projectId" reference="projects">
        <SelectInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
)
export const TaskEdit = (props) => (
  <Edit {...props}>
    <SimpleForm validate={validateTask}>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <TextInput source="description" />
      <ReferenceInput label="Project" source="projectId" reference="projects">
        <SelectInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
)

export const TaskShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="name" />
    </SimpleShowLayout>
  </Show>
)