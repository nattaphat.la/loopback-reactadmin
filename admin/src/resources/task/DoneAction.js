// in src/comment/commentActions.js
import { UPDATE } from 'react-admin';
export const TASK_DONE = 'TASK_DONE';
export const taskDone = (id, data) => ({
    type: TASK_DONE,
    payload: { id, data: { ...data } },
    meta: { resource: 'tasks', fetch: UPDATE },
});