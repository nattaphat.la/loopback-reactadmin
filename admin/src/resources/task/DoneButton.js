import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-admin';
import {taskDone as taskDoneAction } from './DoneAction';
class DoneButton extends Component{
  constructor(props) {
    super(props)
  }
  taskDoneHandler() {
    const {record} = this.props; 
    this.props.taskDone(record.id,{ isDone: true});
  }
  render() {
    return (<Button onClick={e => {this.taskDoneHandler(e)}}>Mark as complete</Button>)
  }
}
export default connect(null, {taskDone: taskDoneAction})(DoneButton);