import React, {Component} from 'react';
import {
  List,
  Datagrid,
  TextField,
  Create,
  SimpleForm,
  TextInput,
  Edit,
  EditButton,
  SimpleShowLayout,
  Show,
  ShowButton,
  DisabledInput,
  ReferenceManyField,
  SingleFieldList,
  ChipField
} from 'react-admin'
export const ProjectList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <ReferenceManyField label="Tasks" reference="tasks" target="projectId">
        <SingleFieldList>
          <ChipField source="name" />
        </SingleFieldList>
      </ReferenceManyField>
      <EditButton></EditButton>
      <ShowButton></ShowButton>
    </Datagrid>
  </List>
)
export const ProjectCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="description" />
    </SimpleForm>
  </Create>
)
export const ProjectEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <TextInput source="description" />
    </SimpleForm>
  </Edit>
)

class MyDataGrid extends Component {
  state = {  }
  render() {
    const {setSort, ...rest} = this.props
    return (
      <Datagrid {...rest}>
        <TextField source="id" />
        <TextField source="name" />
        <TextField source="description" />
        <ShowButton></ShowButton>
        <EditButton></EditButton>
      </Datagrid>
    );
  }
}

export default MyDataGrid;

export const ProjectShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="name" />
      <ReferenceManyField label="Tasks" reference="tasks" target="projectId">
        <MyDataGrid></MyDataGrid>
      </ReferenceManyField>
    </SimpleShowLayout>
  </Show>
)